import React from "react"

class Note extends React.Component {
  render() {
    return (
      <article className="message is-primary">
        <div className="message-header">
          <p></p>
        </div>
        <div className="message-body">{this.props.message}</div>
      </article>
    )
  }
}

export default Note
