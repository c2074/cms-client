import React from "react"
import { Link } from "react-router-dom"

class ClientTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      apiResponse: [],
    }
  }

  async callAPI() {
    const response = await fetch(
      "http://localhost:8080/client/list?selectQuery=all"
    )
    const clients = await response.json()

    this.setState({ apiResponse: clients })
  }

  componentDidMount() {
    this.callAPI()
  }

  render() {
    return (
      <div>
        <table className="table is-hoverable is-fullwidth">
          <tbody>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Phone Number</th>
              <th>Email Address</th>
              <th>Website</th>
            </tr>

            {this.state.apiResponse.map((v, i) => (
              <tr>
                <td>{v.id}</td>
                <td>
                  <Link to={`/viewClient?clientId=${v.id}`}>
                    {v.first_name} {v.last_name}
                  </Link>
                </td>
                <td>{v.phone_number}</td>
                <td>{v.email_address}</td>
                <td>{v.website}</td>
                <td>
                  <a
                    href={`http://localhost:8080/client/delete?id=${v.id}`}
                    className="has-text-danger"
                  >
                    Delete
                  </a>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default ClientTable
