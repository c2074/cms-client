import React from "react"
import { Link } from "react-router-dom"
import Note from "./Note"

class Client extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      clientId: new URLSearchParams(this.props.location.search).get("clientId"),
      notes: [],
    }
  }

  async callAPI() {
    const client_response = await fetch(
      `http://localhost:8080/client/list?selectQuery=id&id=${this.state.clientId}`
    )
    const client = await client_response.json()

    const notes_response = await fetch(
      `http://localhost:8080/note/get?client_id=${this.state.clientId}`
    )

    const notes = await notes_response.json()

    console.log(notes)

    this.setState({
      id: client.id,
      firstName: client.first_name,
      lastName: client.last_name,
      phoneNumber: client.phone_number,
      website: client.website,
      emailAddress: client.email_address,
      updatedAt: client.updatedAt,
      notes: notes,
    })
  }

  componentDidMount() {
    this.callAPI()
  }

  render() {
    // TODO: Line ~65 change links "to" attributes
    return (
      <div className="section columns">
        <div className="column is-four-fifths">
          <div className="level">
            <div className="level-left">
              <div className="level-item">
                <h1 className="title">
                  {this.state.firstName} {this.state.lastName}
                </h1>
              </div>
            </div>
            <div className="level-right">
              <div className="level-item">
                <div className="navbar-item has-dropdown is-hoverable">
                  <Link className="navbar-link" to="#">
                    ACTIONS
                  </Link>
                  <div className="navbar-dropdown">
                    <Link to="/addNote" className="navbar-item">
                      Add Note
                    </Link>
                    <Link to="/" className="navbar-item">
                      Delete Client
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {this.state.notes.map((v, i) => (
            <Note key={v.id} message={v.note} />
          ))}
        </div>
        <div className="column">
          <h3 className="subtitle">Info</h3>
          <p className="is-size-7">Client ID</p>
          <p className="is-size-5">{this.state.id}</p>

          <p className="is-size-7 mt-3">Full Name</p>
          <p className="is-size-5">
            {this.state.firstName} {this.state.lastName}
          </p>
          <p className="is-size-7 mt-3">Email Address</p>
          <p className="is-size-5">{this.state.emailAddress}</p>

          <p className="is-size-7 mt-3">Phone Number</p>
          <p className="is-size-5">{this.state.phoneNumber}</p>

          <p className="is-size-7 mt-3">Website</p>
          <a
            href={`http://${this.state.website}`}
            target="_blank"
            className="is-size-5"
            rel="noopener noreferrer"
          >
            {this.state.website}
          </a>
        </div>
      </div>
    )
  }
}

export default Client
