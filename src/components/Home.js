import React from "react"
import ClientTable from "./ClientTable"

class Home extends React.Component {
  render() {
    return (
      <div class="container">
        <ClientTable />
      </div>
    )
  }
}

export default Home
