import React from "react"

class AddNote extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            clientId: new URLSearchParams(this.props.location.search).get("clientId"),
            clientName: new URLSearchParams(this.props.location.search).get("clientName")
        }
    }

    render() {
        return (
            <div className="container">
                <h1 className="title">Add Note</h1>
                <form action={`http://localhost:8080/note/create?clientId=${this.state.clientId}`} method="GET" id="form">
                    <div className="field">
                        <label className="label">Client Name</label>
                        <input type="text" className="input" value={this.state.clientName} readOnly />
                    </div>
                    <div className="field">
                        <label className="label">Note</label>
                        <div className="control">
                            <textarea className="textarea is-large" form="form" name="note" placeholder="Insert note"></textarea>
                        </div>
                    </div>
                    <div className="field">
                        <div className="control">
                            <input type="submit" className="button is-link" value="Submit" />
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

export default AddNote
