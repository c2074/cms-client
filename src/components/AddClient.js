import React from "react"

class AddClient extends React.Component {
  render() {
    return (
      <div className="container">
        <h1 className="title">Add Client</h1>
        <form action="http://localhost:8080/client/create" method="GET">
          <div className="field">
            <label class="label">First Name*</label>
            <input type="text" className="input" name="firstName" />
          </div>
          <div className="field">
            <label class="label">Last Name*</label>
            <input type="text" className="input" name="lastName" />
          </div>
          <div className="field">
            <label class="label">Phone Number</label>
            <input type="text" className="input" name="phoneNumber" />
          </div>
          <div className="field">
            <label class="label">Email Address</label>
            <input type="text" className="input" name="emailAddress" />
          </div>
          <div className="field">
            <label class="label">Website</label>
            <input type="text" className="input" name="website" />
          </div>
          <div className="field">
            <div className="control">
              <input type="submit" class="button is-link" value="Submit" />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default AddClient
