import "./App.css"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Home from "./components/Home"
import AddClient from "./components/AddClient"
import TopBar from "./components/TopBar"
import Client from "./components/Client"
import AddNote from "./components/AddNote"

function App() {
  return (
    <Router>
      <TopBar />
      <Switch>
        <Route exact path="/viewClient" component={Client} />
        <Route exact path="/addClient">
          <AddClient />
        </Route>
        <Route exact path="/addNote" component={AddNote} />
      <Route exact path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
